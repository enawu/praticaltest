<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Competency extends Model
{
    public function course(){
        return $this->belongsTo(Course::class);
    }
}
