<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    public function learningplan(){
        return $this->belongsTo(LearningPlan::class);
    }

    public function competency(){
        return $this->hasOne(Competency::class);
    }
}
