<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LearningPlan;
use App\Course;
class LearningPlansController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {

        // dd('ll');

        if(request()->ajax())
        {
        
      
            return datatables()->of(LearningPlan::with(['Course','Competency'])->latest()->get())
 
                    ->addColumn('checkbox', function($data){
                        $checkbox = '<input type="checkbox" name="courses[]" class="pdr_checkbox" value="'.$data->id.'" />';
                        return $checkbox;
                    })

                    ->addColumn('closingdate', function($data){
                        if($data->id == ){

                        }
                        $checkbox = '<input type="checkbox" name="courses[]" class="pdr_checkbox" value="'.$data->id.'" />';
                        return $checkbox;
                    })
                    ->make(true);

        }
    
        return view('learnerplan');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
