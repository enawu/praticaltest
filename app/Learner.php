<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Learner extends Model
{
    public function learningplan(){
        return $this->belongsTo(LearningPlan::class);
    }

    public function roles(){
        return $this->hasMany(Role::class);
    }

}
