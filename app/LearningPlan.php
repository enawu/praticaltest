<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LearningPlan extends Model
{

    protected $guarded = [];

    public function learner(){
        return $this->hasOne(Learner::class);
    }

    public function courses(){
        return $this->hasMany(Course::class);
    }

}
