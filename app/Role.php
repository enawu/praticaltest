<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public function learner(){
        return $this->belongsTo(Learner::class);
    }
}
