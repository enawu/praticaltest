@extends('layouts.app')
@section('title','Learner Plan')
@section('content')



<div class="row">
        <div style="margin-left :10%;margin-right :10%;" >
          <div class="panel panel-default">
            <div class="panel-heading " >
                <div  >
                    <h3 class="panel-title "> Your Learning Plan  </h3>
                </div>
                <div >
                    <!-- <p><a class ="btn btn-primary" href="loads/create">Submit Load</a></p> -->
                </div>                
            </div>
            <div class="panel-body">

            <div class="table-responsive">
            <table class="table table-bordered table-striped" id="learningplan_table">
            <thead >
                <tr>
                  <th scope="col">Competency</th>
                  <th scope="col">Course Name</th>
                  <th scope="col">ID</th>
                  <th scope="col">Type</th>
                  <th scope="col">H:Min</th>
                  <th scope="col">Key</th>
                  <th scope="col">Description</th>
                  <th scope="col">Completed Planned Date</th>
                </tr>
            </thead>
        
            </table>  
            </div> 
            </div>
          </div>
        </div>
      </div>



   
  <script>
$(document).ready(function(){
    
 $('#learningplan_table').DataTable({
  processing: true,
  serverSide: true,
  ajax:{
   url: "{{ route('learnerplans.index') }}",
  },

 });

});
</script>
@endsection